#include <stdio.h>
#include <string.h>

#define MAX_LENGTH 50

int count_char(char *string, char c);
int count_words(char *string, char *word);
int count_words_better(char *string, char *word);

int main() {
    char string[MAX_LENGTH];
    printf("Entrez une chaine de caracteres : ");
    if (fgets(string, MAX_LENGTH, stdin) == NULL) {
        fprintf(stderr, "Erreur de saisie.\n");
        return 1;
    }
    string[strlen(string)-1] = '\0';

    char c = 'o';
    int count = count_char(string, c);
    printf("Le caractere '%c' apparait %d fois dans la chaine.\n", c, count);

    char word[] = "be";
    count = count_words(string, word);
    printf("Le mot '%s' apparait %d fois dans la chaine.\n", word, count);

    count = count_words_better(string, word);
    printf("Le mot '%s' apparait %d fois dans la chaine (version amelioree).\n", word, count);

    return 0;
}

int count_char(char *string, char c) {
    int count = 0;
    int i = 0;
    while (string[i] != '\0') {
        if (string[i] == c) {
            count++;
        }
        i++;
    }
    return count;
}

int count_words(char *string, char *word) {
    int count = 0;
    char *token = strtok(string, " ");
    while (token != NULL) {
        if (strcmp(token, word) == 0) {
            count++;
        }
        token = strtok(NULL, " ");
    }
    return count;
}

int count_words_better(char *string, char *word) {
    int count = 0;
    int string_length = strlen(string);
    int word_length = strlen(word);
    if (string_length < word_length) {
        return 0;
    }
    char *p = string;
    while (p < string + string_length - word_length + 1) {
        if (strncmp(p, word, word_length) == 0) {
            count++;
            p += word_length;
        } else {
            p++;
        }
        while (*p == ' ') {
            p++;
        }
    }
    return count;
}

