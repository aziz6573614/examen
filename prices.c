#include <stdio.h>

int main() {
    float prix1, prix2;
    float pourcentage;

    // Demander à l'utilisateur de saisir les prix
    printf("prix 1 : ");
    scanf("%f", &prix1);
    printf("prix 2 : ");
    scanf("%f", &prix2);

    // Calculer le pourcentage d'augmentation ou de baisse
    pourcentage = ((prix2 - prix1) / prix1) * 100;

    // Afficher le pourcentage avec un signe "+" ou "-"
    if (pourcentage >= 0) {
        printf(" augmentation de : +%0.2f%%\n", pourcentage);
    } else {
        printf(" baisse de : %0.2f%%\n", pourcentage);
    }

    return 0;
}
