#include <stdio.h>

void small_to_zero(int *t, int n, int val) {
    for (int i = 0; i < n; i++) {
        if (t[i] <= val) {
            t[i] = 0;
        }
    }
}

int main() {
    int tab[] = { 1, 3, -1, 2, 7, 5, 12, 4, 4, 3, 0 };
    int n = sizeof(tab) / sizeof(tab[0]);

    printf("Tableau  :\n");
    for (int i = 0; i < n; i++) {
        printf("%d ", tab[i]);
    }
    printf("\n");

    int val = 5;
    small_to_zero(tab, n, val);

    printf("Tableau val=%d :\n", val);
    for (int i = 0; i < n; i++) {
        printf("%d ", tab[i]);
    }
    printf("\n");

    return 0;
}
