#include<stdio.h>
#include<stdlib.h>
#include<math.h>

void add_vectors(int n, double *t1, double *t2, long double *t3) {
    for (int i = 0; i < n; i++) {
        t3[i] = pow((t1[i] - t2[i]), 2);
    }
}

double norm_vector(int n, double *t) {
    long double sum = 0.0;
    for (int i = 0; i < n; i++) {
        sum += pow(t[i], 2);
    }
    return sqrt(sum);
}

int main() {
    double T1[] = { 3.14, -1.0, 2.3, 0, 7.1 };
    double T2[] = { 2.71,  2.5,  -1, 3, -7  };
    long double T3[5];
    int n = 5;

    printf("T1 : ");
    for (int i = 0; i < n; i++) {
        printf("%+.2lf ", T1[i]);
    }
    printf("\nT2 : ");
    for (int i = 0; i < n; i++) {
        printf("%+.2lf ", T2[i]);
    }
    printf("\n");

    add_vectors(n, T1, T2, T3);
    printf("T3 = (T1 - T2)^2 : ");
    for (int i = 0; i < n; i++) {
        printf("%+.2Lf ", T3[i]);
    }
    printf("\n");

    double norm_T1 = norm_vector(n, T1);
    printf("Norme euclidienne de T1 : %f\n", norm_T1);

    double norm_T2 = norm_vector(n, T2);
    printf("Norme euclidienne de T2 : %f\n", norm_T2);

    return 0;
}


