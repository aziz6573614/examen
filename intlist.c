
#include <stdio.h>
#include <stdlib.h>

typedef struct node node;

struct node {
    int val;
    node *next;
    node *prev; // nouveau champ pour le précédent
};

node *create_node(int val) {
    node *p = malloc(sizeof(node));
    p->val = val;
    p->next = NULL;
    p->prev = NULL; // initialise le champ prev à NULL
    return p;
}

node *append_val(node *head, int val) {
    node *newnode = create_node(val);

    if (head == NULL) {
        head = newnode;
    } else {
        node *walk = head;
        while (walk->next != NULL) {
            walk = walk->next;
        }
        walk->next = newnode;
        newnode->prev = walk; // le nouveau noeud pointe vers le précédent
    }
    return head;
}

void print_list(node *head) {
    node *walk = head;
    while (walk != NULL) {
        printf("%d ", walk->val);
        walk = walk->next;
    }
    printf("\n");
}

void print_list_reverse(node *tail) {
    node *walk = tail;
    while (walk != NULL) {
        printf("%d ", walk->val);
        walk = walk->prev;
    }
    printf("\n");
}

void forth_and_back(node *head) {
    printf("Liste du premier au dernier : ");
    print_list(head);

    node *tail = head;
    while (tail->next != NULL) {
        *tail = *tail->next;
    }

    printf("Liste du dernier au premier : ");
    print_list_reverse(tail);
}

int main() {
    node *head = NULL;

    head = append_val(head, 42);
    head = append_val(head, 12);
    head = append_val(head, 54);
    head = append_val(head, 41);

    forth_and_back(head);

    return 0;
}


